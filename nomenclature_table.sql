CREATE TABLE [dbo].[nomenclature](
	[id] [int] IDENTITY(0,1) NOT NULL,
	[table] [nvarchar](max) NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[description] [nvarchar](max) NULL,
	[date_debut] [datetime] NULL,
	[date_fin] [datetime] NULL,
	[usta] [nvarchar](50) NULL,
	[uspe] [nvarchar](50) NULL,
	[isleaf] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO