import { Directive, OnInit, HostListener, Input, Output, EventEmitter, ElementRef } from "@angular/core";

/**
 * Appelle la fonction 'trigger' dès que la scrollbar dépasse un certain seuil.
 * Pour ce faire: 
 *  - Ecoute l'événement HTML 'scroll' de l'élément sur lequel la directive est appliquée.
 *  - Utilise les propriétés HTML 'scrollTop', 'scrollHeight' et 'offsetHeight' pour déterminer
 *    si le seuil est franchi.
 */
@Directive({
    selector: '[scrollable]'
})
export class ScrollableDirective implements OnInit {
    
    /**
     * Détermine à partir de quel pourcentage d'avancée du scroll l'événement 'trigger' est déclenché.
     * Valeur entre 0 et 1 (exemple 0.6 <=> 60%)
     */
    @Input() triggerPercent: number;
    @Output('trigger') onScroll = new EventEmitter();

    private el: HTMLElement;

    constructor(elementRef: ElementRef) {
        this.el = elementRef.nativeElement;
    }

    ngOnInit(): void {
        if (this.triggerPercent < 0) {
            this.triggerPercent = 0;
        } else if (this.triggerPercent > 1) {
            this.triggerPercent = 1;
        }
    }

    @HostListener('scroll')
    public handleScroll() {
        const { scrollTop, scrollHeight, offsetHeight } = this.el;
        const maxScrollTop = scrollHeight - offsetHeight;

        if ( scrollTop >= this.triggerPercent * maxScrollTop ) {
            this.onScroll.emit('');
        }
    }
}