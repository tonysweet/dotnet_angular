import { Component } from '@angular/core';
import { NomenclatureService } from '../services/nomenclature.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { NomenclatureDetails } from '../models/nomenclature';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.scss']
})
export class NavMenuComponent implements OnInit {
  public isExpanded = false;
  public nomenDetails: NomenclatureDetails = null;

  constructor(private nomenclatureService: NomenclatureService) {
  }

  ngOnInit() {
    this.nomenclatureService.nomenclatureDetails$.subscribe((n) => {
      this.nomenDetails = n;
    })
  }

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
}
