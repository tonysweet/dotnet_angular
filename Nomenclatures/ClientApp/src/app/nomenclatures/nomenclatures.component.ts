import { Component, OnInit } from '@angular/core';
import { NomenclatureSearchQuery, NomenclatureService } from '../services/nomenclature.service';
import { Nomenclature, NomenclatureDTO } from '../models/nomenclature';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { debounceTime, distinctUntilChanged, finalize, switchMap, map } from 'rxjs/operators';
import { merge } from 'rxjs/observable/merge';

class NomenclatureUI extends Nomenclature {
    isSelected: boolean;

    constructor(dto: NomenclatureDTO) {
        super(dto);
        this.isSelected = false;
    }
}

@Component({
  selector: 'nomenclatures',
  templateUrl: './nomenclatures.component.html',
  styleUrls: ['./nomenclatures.component.scss']
})
export class NomenclaturesComponent implements OnInit {
    private readonly searchSize = 60;
    private searchFrom: number = 0;
    private hasMoreData: boolean = true;
    private codePredicate: string;
    private isSearching: boolean;
    private readonly searchInputSource = new ReplaySubject<string>();
    private readonly searchTriggerSource = new ReplaySubject<boolean>();

    public nomenclatures: Array<NomenclatureUI> = [];
    public nomenclatureSelectedIds: Array<number> = [];
    public isSortASC = true;

    constructor(private nomenclatureService: NomenclatureService) {
    }

    ngOnInit(): void {
        this.initSearchPipeline();
        this.searchTriggerSource.next(true);
    }

    private getInputPipeline(): Observable<boolean> {
        return this.searchInputSource.pipe(
            debounceTime(200), 
            distinctUntilChanged(),
            map((v) => {
                this.codePredicate = v;
                return true;
            })
        )
    }

    private initSearchPipeline() {
        merge(
            this.getInputPipeline(),
            this.searchTriggerSource
        )
        .pipe(
            switchMap((shouldReset: boolean) => {
                return this.search(shouldReset)
            })
        )
        .subscribe((dtoArray: Array<NomenclatureDTO>) => {
            this.nomenclatures.push(...dtoArray.map<NomenclatureUI>(dto => new NomenclatureUI(dto)));
            this.hasMoreData = this.searchSize == dtoArray.length;
        })
    }

    private search(shouldReset: boolean = true): Observable<NomenclatureDTO[]> {
        this.isSearching = true;
        console.debug("Start SEARCHING");

        if (shouldReset) {
            this.resetSearch();
        }

        return this.nomenclatureService.search(<NomenclatureSearchQuery>{
            codePredicate: this.codePredicate,
            isASC: this.isSortASC,
            from: this.searchFrom,
            size: this.searchSize
        })
        .pipe(
            finalize(() => {
                this.isSearching = false;
                console.debug("END SEARCHING");
            })
        )
    }

    private resetSearch(): void {
        this.nomenclatures = [];
        this.nomenclatureSelectedIds = [];
        this.searchFrom = 0;
        this.hasMoreData = true;
    }

    public onInputChange(value: string): void {
        this.searchInputSource.next(value)
    }

    public select(n: NomenclatureUI): void {
        if (n.isLeaf) {
            n.isSelected = !n.isSelected;
            if (n.isSelected) {
                this.nomenclatureSelectedIds.push(n.id);
            } else {
                this.nomenclatureSelectedIds = this.nomenclatureSelectedIds.filter(id => id != n.id);
            }
        }
    }

    public toogleSort(): void {
        this.isSortASC = !this.isSortASC;
        this.searchTriggerSource.next(true);
    }

    public loadMore(): void {
        console.debug("SCROLL LOAD MORE");

        if (!this.isSearching && this.hasMoreData) {
            this.searchFrom += this.searchSize;
            this.searchTriggerSource.next(false);
        }
    }

    public setDetails(id: number): void {
        this.nomenclatureService.setCurrentNomenclature(id);
    }
}