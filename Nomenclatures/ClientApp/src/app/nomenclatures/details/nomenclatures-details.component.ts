import { Component, Input, OnInit, SimpleChanges, OnChanges } from "@angular/core";
import { NomenclatureDetails } from "../../models/nomenclature";

@Component({
    selector: "nomenclature-details",
    templateUrl: "./nomenclatures-details.component.html",
    styleUrls: ["nomenclatures-details.component.scss"]
})
export class NomenclatureDetailsComponent implements OnInit, OnChanges {
    
    @Input('data') nomenDetails: NomenclatureDetails = null;

    constructor() {}

    ngOnInit(): void {
        this.handleEmptyProps();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.handleEmptyProps();
    }

    private handleEmptyProps() {
        for (const key in this.nomenDetails) {
            if (this.nomenDetails[key] === null || this.nomenDetails[key] == "") {
                this.nomenDetails[key] = "N/A";
            }
        }
    }
}
