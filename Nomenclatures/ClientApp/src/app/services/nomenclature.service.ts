import { Injectable, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import * as QueryString from "query-string";
import { Nomenclature, NomenclatureDTO, NomenclatureDetails } from "../models/nomenclature";
import { HttpParams } from "@angular/common/http/src/params";
import { API_URL } from "../config.constants";
import { ReplaySubject } from "rxjs/ReplaySubject";

export interface NomenclatureSearchQuery {
    codePredicate: string;
    sortField: string;
    isASC: boolean;
    from: number;
    size: number;
}

export interface INomenclatureService {
    search(query: NomenclatureSearchQuery): Observable<Array<NomenclatureDTO>>;
    getById(id: number): Observable<NomenclatureDTO>;
    getCurrentNomenclature(): NomenclatureDetails;
    setCurrentNomenclature(id: number): void;
}

@Injectable()
export class NomenclatureService implements INomenclatureService {

    private readonly ApiUrl: string;
    private _nomenclatureDetails$ = new ReplaySubject<NomenclatureDetails>();

    public _currentNomenclature: NomenclatureDetails;

    constructor(private $http: HttpClient, @Inject(API_URL) apiUrl: string) {
        this.ApiUrl = apiUrl;
    }

    getById(id: number): Observable<NomenclatureDTO> {
        return this.$http.get<NomenclatureDTO>(`${this.ApiUrl}/nomenclatures/${id}`);
    }

    search(query: NomenclatureSearchQuery): Observable<Array<NomenclatureDTO>> {
        for (const key in query) {
            if (query[key] == null) {
                delete query[key];
            }
        }

        const params = QueryString.stringify(query);
        return this.$http.get<Array<NomenclatureDTO>>(`${this.ApiUrl}/nomenclatures?${params}`);
    }

    getCurrentNomenclature(): NomenclatureDetails {
        return this._currentNomenclature;
    }

    setCurrentNomenclature(id: number): void {
        this.getById(id).subscribe((n: NomenclatureDTO) => {
            this._currentNomenclature = new NomenclatureDetails(n);
            this._nomenclatureDetails$.next(this._currentNomenclature);
        });
    }

    public get nomenclatureDetails$() {
        return this._nomenclatureDetails$.asObservable();
    }
}