import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { AboutComponent } from './about/about.component';
import { NomenclatureService } from './services/nomenclature.service';
import { NomenclaturesComponent } from './nomenclatures/nomenclatures.component';
import { ScrollableDirective } from './attrs-directives/scrollable.directive';
import { NomenclatureDetailsComponent } from './nomenclatures/details/nomenclatures-details.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    AboutComponent,
    NomenclaturesComponent,
    NomenclatureDetailsComponent,
    ScrollableDirective
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: '/nomenclatures', pathMatch: 'full' },
      { path: 'nomenclatures', component: NomenclaturesComponent},
      { path: 'about', component: AboutComponent}
    ])
  ],
  providers: [
    NomenclatureService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
