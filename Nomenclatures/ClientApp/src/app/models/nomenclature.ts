export interface NomenclatureDTO {
    id: number;
    code: number;
    description: string;
    isLeaf: boolean;
    table: string;
    date_Debut: Date;
    date_Fin: Date;
    usta: string;
    uspe: string;
}

export class Nomenclature {
    id: number;
    code: number;
    description: string;
    isLeaf: boolean;

    constructor(dto: NomenclatureDTO) {
        this.id = dto.id
        this.code = dto.code
        this.description = dto.description
        this.isLeaf = dto.isLeaf
    }
}

export class NomenclatureDetails extends Nomenclature {
    table: string;
    dateDebut: Date;
    dateFin: Date;
    usta: string;
    uspe: string;

    constructor(dto: NomenclatureDTO) {
        super(dto);
        this.table = dto.table
        this.dateDebut = new Date(dto.date_Debut);
        this.dateFin = new Date(dto.date_Fin);
        this.usta = dto.usta
        this.uspe = dto.uspe
    }
}

