using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Nomenclatures.Data;
using Nomenclatures.Models;

namespace Nomenclatures.Controllers
{
    [Route("api/nomenclatures")]
    public class NomenclatureController : Controller
    {
        private AppDbContext context;
        public NomenclatureController(AppDbContext context) {
            this.context = context;
        }

        [HttpGet]
        public List<Nomenclature> SearchNomenclatures([FromQuery] SearchQuery inputQuery)
        {
            var nomenclatureIQ = context.Nomenclatures.Select(n => new Nomenclature{
                ID = n.ID,
                Code = n.Code,
                Description = n.Description,
                isLeaf = n.isLeaf
            });

            if ( !string.IsNullOrEmpty(inputQuery.CodePredicate) ) {
                nomenclatureIQ = nomenclatureIQ.Where(n => n.Code.StartsWith(inputQuery.CodePredicate));
            }

            if (inputQuery.isASC) {
                nomenclatureIQ = nomenclatureIQ.OrderBy(n => n.Code);
            }
            else {
                nomenclatureIQ = nomenclatureIQ.OrderByDescending(n => n.Code);
            }

            return nomenclatureIQ
                .Skip(inputQuery.From)
                .Take(inputQuery.GetEffectiveSize())
                .ToList();                
        }

        [HttpGet("{id}")]
        public Nomenclature GetNomenclatureById(int id)
        {
            return context.Nomenclatures.FirstOrDefault(n => n.ID == id);
        }
    }

    public class SearchQuery {
        public string CodePredicate { get; set; }
        public string SortField { get; set; } = "Code";
        public bool isASC { get; set; }
        public int From { get; set; }
        public int DefaultSize { get; private set; } = 30;
        public int MaxSize { get; private set; } = 120;
        public int Size { get; set; }
        public int GetEffectiveSize() {
            if (Size < DefaultSize) {
                return DefaultSize;
            } else if (Size > MaxSize) {
                return MaxSize;
            } else {
                return Size;
            }
        }
    }
}
