using CsvHelper;

namespace Nomenclatures.Services
{
    public interface ICSVService
    {
        CsvReader getNomenclaturesReader();
    }
}