using Nomenclatures.Models;

namespace Nomenclatures.Services
{
    public interface INomenclatureCSVReaderService
    {
        // Importe les nomenclatures en base de données 
        // et retourne le nombre de nomenclatures importées
        long ImportNomenclatures();

        bool IsCurrentNomenclatureALeaf(Nomenclature current, Nomenclature next);
    }
}