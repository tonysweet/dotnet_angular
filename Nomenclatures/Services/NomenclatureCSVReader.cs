using System;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using Microsoft.Extensions.Logging;
using Nomenclatures.Data;
using Nomenclatures.Models;

namespace Nomenclatures.Services
{
    public class NomenclatureCSVReaderService : INomenclatureCSVReaderService
    {
        private ICSVService csvService;
        private AppDbContext context;
        private ILogger LOGGER;
        public NomenclatureCSVReaderService(ICSVService csvService,
                                            AppDbContext context,
                                            ILogger<NomenclatureCSVReaderService> logger) {
            this.csvService = csvService;
            this.context = context;
            this.LOGGER = logger;
        }

        public long ImportNomenclatures() {
            LOGGER.LogInformation("Importation des nomenclatures ...");
            long nomenclatureCount = 0;

            var csvReader = csvService.getNomenclaturesReader();
            // Lit la 1ère ligne (en-têtes)
            csvReader.Read();
            csvReader.ReadHeader();
            // Lit la 1ère Nomenclature
            csvReader.Read();
            Nomenclature current = csvReader.GetRecord<Nomenclature>();

            while( csvReader.Read() )
            {
                Nomenclature next = csvReader.GetRecord<Nomenclature>();

                current.isLeaf = IsCurrentNomenclatureALeaf(current, next);

                // LOGGER.LogDebug($"Count {nomenclatureCount}");

                // Prépare pour la sauvegarde
                context.Nomenclatures.Add(current);
                
                current = next;
                nomenclatureCount++;
            }

            // La dernière nomenclature est une feuille
            current.isLeaf = true;
            // Prépare pour la sauvegarde le dernier élément
            context.Nomenclatures.Add(current);
            // Save all
            context.SaveChanges();

            LOGGER.LogInformation($"Fin de l'importation. ( {++nomenclatureCount} importées)");
            return nomenclatureCount;
        }

        // Détermine si la nomenclature courante est une feuille (true) ou pas (false)
        // S'appuie sur le fait que les nomenclatures sont triés par code ASC
        // Voir la classe de Tests associées pour des exemples
        public bool IsCurrentNomenclatureALeaf(Nomenclature current, Nomenclature next)
        {
            var nextCode = Int32.Parse(next.Code);
            var currentCode = Int32.Parse(current.Code);
            var codeDiff = nextCode - currentCode;

            if ((codeDiff == 1 || codeDiff == 10 || codeDiff == 100)
                && (currentCode % (codeDiff * 10) == 0)) 
            {
                return false;
            } else if (codeDiff >= 1000 && codeDiff < 10000 && currentCode % 10000 == 0) {
                return false;
            } else {
                return true;
            }
        }
    }
}