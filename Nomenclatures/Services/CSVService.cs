using System.Globalization;
using System.IO;
using CsvHelper;
using CsvHelper.TypeConversion;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using Nomenclatures.Models;

namespace Nomenclatures.Services {
    public class CSVService : ICSVService
    {
        private string nomenclatureFilePath;
        private ILogger LOGGER;
        public CSVService(IConfiguration Configuration, ILogger<CSVService> logger)
        {
            nomenclatureFilePath = Configuration["NomenclaturesCSVFile"];
            LOGGER = logger;
        }

        public CsvReader getNomenclaturesReader()
        {
            StreamReader reader = File.OpenText(nomenclatureFilePath);
            var csvReader = new CsvReader(reader);
            // Délimiteur CSV
            csvReader.Configuration.Delimiter = "|";
            // Format des dates
            var typeConverterOpts = new TypeConverterOptions();
            typeConverterOpts.CultureInfo = new CultureInfo("fr-FR");
            csvReader.Configuration.TypeConverterOptionsCache.AddOptions(typeof(Nomenclature), typeConverterOpts);
            // Ignore les champs manquants, utile pour isLeaf.
            csvReader.Configuration.MissingFieldFound = null;
            // Ignore les quotes
            csvReader.Configuration.IgnoreQuotes = true;
            // Log bad data.
            csvReader.Configuration.BadDataFound = context => {
                LOGGER.LogInformation($"Bad data found on row '{context.RawRow}'");
            };
            csvReader.Configuration.ReadingExceptionOccurred = exception =>
            {
                LOGGER.LogInformation( $"Reading exception: {exception.Message}" );
                throw exception;
            };
            return csvReader;
        }
    }
}