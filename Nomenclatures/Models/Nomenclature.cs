
using System;

namespace Nomenclatures.Models {
    public class Nomenclature {
        public Nomenclature()
        {
        }

        public int ID { get; set; }
        public string Table { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public DateTime Date_Debut { get; set; }
        public DateTime Date_Fin { get; set; }
        public string USTA { get; set; }
        public string USPE { get; set; }
        public bool isLeaf { get; set; }
    }
}