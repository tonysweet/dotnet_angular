using Microsoft.EntityFrameworkCore;
using Nomenclatures.Models;

namespace Nomenclatures.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        public DbSet<Nomenclature> Nomenclatures { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Nomenclature>().ToTable("Nomenclature");
        }
    }
}