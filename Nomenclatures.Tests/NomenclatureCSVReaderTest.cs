using System;
using Xunit;
using CsvHelper;
using Nomenclatures.Models;
using Nomenclatures.Services;

namespace Nomenclatures.Tests
{
    public class NomenclatureCsvReaderTest
    {
        private readonly INomenclatureCSVReaderService service;

        public NomenclatureCsvReaderTest()
        {
            // TODO Mock les dépendences
            service = new NomenclatureCSVReaderService(null, null, null);
        }

        [Theory]
        // Différence dans [1000; max[ 
        [InlineData(01010000, 01012000, false)]
        [InlineData(01040000, 01041000, false)]
        [InlineData(60010000, 60011000, false)]
        [InlineData(86080000, 86090000, true)]
        [InlineData(86090000, 87010000, true)]
        [InlineData(87011000, 87012000, true)]
        [InlineData(87013000, 87019000, true)]
        [InlineData(59119000, 60010000, true)]
        // Différence dans [100; 1000[
        [InlineData(01012000, 01012100, false)]
        [InlineData(01012100, 01012900, true)]
        [InlineData(01012900, 01013000, true)]
        [InlineData(01041090, 01042000, true)]
        [InlineData(01051199, 01051200, true)]
        [InlineData(01051200, 01051300, true)]
        [InlineData(52053000, 52053100, false)]
        [InlineData(52053100, 52053200, true)]
        [InlineData(52053500, 52054000, true)]
        // Différence dans [10; 100[ 
        [InlineData(01041000, 01041010, false)]
        [InlineData(01041010, 01041090, true)]
        [InlineData(01051119, 01051190, true)]
        [InlineData(87021000, 87021010, false)]
        [InlineData(87021010, 87021020, true)]
        // Différence dans [1; 10[ 
        [InlineData(01051110, 01051111, false)]
        [InlineData(01051111, 01051119, true)]
        [InlineData(87033210, 87033211, false)]
        [InlineData(87033211, 87033219, true)]
        [InlineData(87033219, 87033220, true)]
        public void DoitRetournerVraiSiLaNomenclatureEstUneFeuille(int codeCurrent, int codeNext, bool isLeaf)
        {
            var current = MakeNomenclatureByCode(codeCurrent);
            var next = MakeNomenclatureByCode(codeNext);

            Assert.Equal(isLeaf, service.IsCurrentNomenclatureALeaf(current, next));
        }

        private Nomenclature MakeNomenclatureByCode(int code) {
            var n = new Nomenclature();
            n.Code = "" + code;
            return n;
        }
    }
}
